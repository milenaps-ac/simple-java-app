FROM maven:3.3-jdk-8-onbuild

FROM openjdk:8-jdk-alpine
COPY --from=0 /usr/src/app/target/example-0.0.1-SNAPSHOT.jar /opt/simple-java-app.jar
ARG JAVA_OPTS
ENV JAVA_OPTS ${JAVA_OPTS}
CMD java $JAVA_OPTS -jar /opt/simple-java-app.jar