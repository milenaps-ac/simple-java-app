# Simple Spring Boot Application

### Pre-requisites
1. Clone this repository;
2. Download and install the [JDK](http://www.oracle.com/technetwork/java/javase/downloads) latest version;
3. Preferrably use some IDE such as [Eclipse](http://www.eclipse.org/downloads) or [IntelliJ](http://www.jetbrains.com/idea/download), otherwise, you'll need [Maven](http://maven.apache.org/download.cgi) installed and configured on your machine.


### Running the application

--From your IDE or command line using maven:
1. In your IDE, import the app as Maven project for downloading and configuring dependencies, but if you opted by not using an IDE, go to the project root folder via command line and run "mvn clean package";
2. Having compiled/installed the project successfully, run it as 'Java Application' choosing the main class "com.avenuecode.blog.example.Application", and in case you're doing it the hardest way, test "java -Dspring.profiles.active=dev -jar target/example-0.0.1-SNAPSHOT.jar";
3. If no exception popped up in your console, finally test invoking http://localhost:8080/api/examples to see sample results.

--Using docker
1. In the project root folder run: "docker build -t com.avenuecode.blog.example:dev --build-arg JAVA_OPTS=-Dspring.profiles.active=dev ."
2. Then run "docker run -d -p 8080:8080 com.avenuecode.blog.example:dev"
3. Check sample results invoking "http://localhost:8080/api/examples"
