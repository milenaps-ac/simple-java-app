package com.avenuecode.blog.example.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.avenuecode.blog.example.model.Example;

@Service
public class ExampleService {

	public List<Example> listAll() {
		//TODO Implement DAO and fetch data from it
		List<Example> examples = new ArrayList<Example>();
		Example ex = new Example();
		ex.setId("adcg-13gh-1234-9876");
		ex.setName("example1");
		examples.add(ex);

		ex = new Example();
		ex.setId("bdcg-13gh-1234-9876");
		ex.setName("example2");
		examples.add(ex);

		ex = new Example();
		ex.setId("cdcg-13gh-1234-9876");
		ex.setName("example3");
		examples.add(ex);

		return examples;
	}
}
