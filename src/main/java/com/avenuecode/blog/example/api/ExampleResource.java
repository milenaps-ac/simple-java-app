package com.avenuecode.blog.example.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.avenuecode.blog.example.model.Example;
import com.avenuecode.blog.example.service.ExampleService;

@RestController
@RequestMapping("/api/examples")
public class ExampleResource {

	@Autowired
	private ExampleService service;

	@RequestMapping(method = RequestMethod.GET)
	public List<Example> listAll() {
		return service.listAll();
	}
}
